#!/bin/sh

docker run -d --name redis-test -p 30002:6379 redis:6.2.5-alpine3.14

sleep 5

python -W ignore::DeprecationWarning -m unittest

docker stop redis-test && docker rm redis-test